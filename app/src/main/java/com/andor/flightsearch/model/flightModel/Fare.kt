package com.andor.flightsearch.model.flightModel

data class Fare(
    val fare: Int,
    val providerId: Int
)